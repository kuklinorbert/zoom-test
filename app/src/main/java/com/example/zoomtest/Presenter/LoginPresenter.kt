package com.example.zoomtest.Presenter

import com.example.zoomtest.Contract.LoginContract
import com.example.zoomtest.Model.LoginModel
import com.google.firebase.auth.FirebaseUser

class LoginPresenter (private val loginView : LoginContract.View) : LoginContract.Presenter, LoginContract.Model.LoginListener {

    val loginModel : LoginContract.Model = LoginModel()


    override fun login(email: String, password: String) {
        loginModel.loginUser(this, email,password)
    }

    override fun onSuccess(firebaseUser: FirebaseUser?) {
        loginView.loginSuccess(firebaseUser!!)
    }

    override fun onFailure(message: String) {
        loginView.loginFail(message)
    }
}