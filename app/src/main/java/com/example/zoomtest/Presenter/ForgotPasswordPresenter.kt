package com.example.zoomtest.Presenter

import com.example.zoomtest.Contract.ForgotPasswordContract
import com.example.zoomtest.Contract.LoginContract
import com.example.zoomtest.Model.ForgotPasswordModel
import com.google.firebase.auth.FirebaseUser

class ForgotPasswordPresenter (private val passView : ForgotPasswordContract.View) : ForgotPasswordContract.Presenter, ForgotPasswordContract.Model.LoginListener {
    private val passModel: ForgotPasswordContract.Model = ForgotPasswordModel()

    override fun onSuccess() {
        passView.sendSuccess()
    }

    override fun onFailure(message: String) {
        passView.sendFail(message)
    }

    override fun resetPassword(email: String) {
        passModel.sendPasswordReset(this,email)
    }

}