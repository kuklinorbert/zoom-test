package com.example.zoomtest.Presenter


import com.example.zoomtest.Contract.RegisterContract
import com.example.zoomtest.Model.RegisterModel
import com.google.firebase.auth.FirebaseUser
import java.util.*

class RegisterPresenter (private val regView : RegisterContract.View) : RegisterContract.Presenter, RegisterContract.Model.RegisterListener {

    private var regModel : RegisterContract.Model = RegisterModel()


    override fun onSuccess(firebaseUser: FirebaseUser?) {
        regView.registerSuccess(firebaseUser!!)
    }

    override fun onFailure(message: String) {
        regView.registerFail(message)
    }

    override fun register(
        email: String,
        password: String,
        name: String,
        phoneNumber: String,
        birthDate: Date
    ) {
        regModel.registerUser(this, email,password,name,phoneNumber,birthDate)
    }
}