package com.example.zoomtest.Presenter

import com.example.zoomtest.Contract.UserContract
import com.example.zoomtest.Model.User
import com.example.zoomtest.Model.UserModel

class UserPresenter (private val userView: UserContract.View) : UserContract.Presenter, UserContract.Model.UserDataListener {

    private var userModel : UserContract.Model = UserModel()

    override fun onSuccess(user: User) {
        userView.getDataSuccess(user)
    }

    override fun onFailure(message: String) {
        userView.getDataFail(message)
    }

    override fun getData(uid: String) {
        userModel.getUserData(this, uid)
    }
}