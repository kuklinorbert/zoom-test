package com.example.zoomtest.Model

import com.example.zoomtest.Contract.ForgotPasswordContract
import com.google.firebase.auth.FirebaseAuth

class ForgotPasswordModel : ForgotPasswordContract.Model {
    override fun sendPasswordReset(
        listener: ForgotPasswordContract.Model.LoginListener,
        email: String
    ) {
        FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener {
            if (it.isSuccessful) {
                listener.onSuccess()
            } else {
                listener.onFailure(it.exception?.message.toString())
            }
        }
    }

}