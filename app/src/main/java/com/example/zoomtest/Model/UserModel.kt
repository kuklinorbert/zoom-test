package com.example.zoomtest.Model

import com.example.zoomtest.Contract.UserContract
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.FirebaseFirestore
import java.sql.Timestamp
import java.util.*

class UserModel : UserContract.Model {
    override fun getUserData(listener: UserContract.Model.UserDataListener, uid: String) {
        FirebaseFirestore.getInstance().collection("UserData").document(uid).get().addOnSuccessListener {
            if(it != null){
                var timestamp = it.data?.get("birthDate") as com.google.firebase.Timestamp
                println(timestamp)
                var date = timestamp.toDate()
                println(date)
                listener.onSuccess(
                    User(
                        name = it.data?.get("name") as String,
                        email = it.data?.get("email") as String,
                        phoneNumber = it.data?.get("phoneNumber") as String,
                        birthDate = date
                    )
                )
            }else{
                listener.onFailure("Could not find user!")
            }
        }.addOnFailureListener{
            listener.onFailure(it.message.toString())
        }
    }
}