package com.example.zoomtest.Model

import com.example.zoomtest.Contract.RegisterContract
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class RegisterModel : RegisterContract.Model {



    override fun registerUser(
        listener: RegisterContract.Model.RegisterListener,
        email: String,
        password: String,
        name: String,
        phoneNumber: String,
        birthDate: Date
    ) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password).addOnCompleteListener(
            OnCompleteListener {
                if(it.isSuccessful){
                    val userData = hashMapOf(
                        "uid" to it.result?.user?.uid,
                        "name" to name,
                        "email" to email,
                        "phoneNumber" to phoneNumber,
                        "birthDate" to birthDate
                    )
                    FirebaseFirestore.getInstance().collection("UserData").document(it.result!!.user!!.uid).set(userData)
                    listener.onSuccess(it.result!!.user)
                }else{
                    listener.onFailure(it.exception?.message.toString())
                }
            })
    }
}