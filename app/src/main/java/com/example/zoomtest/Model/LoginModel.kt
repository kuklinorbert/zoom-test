package com.example.zoomtest.Model

import com.example.zoomtest.Contract.LoginContract
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth

class LoginModel : LoginContract.Model{
    override fun loginUser(
        listener: LoginContract.Model.LoginListener,
        email: String,
        password: String
    ) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password).addOnCompleteListener(
            OnCompleteListener {
                if(it.isSuccessful){
                    listener.onSuccess(it.result!!.user)
                }else{
                    listener.onFailure(it.exception?.message.toString())
                }
            }
        )
    }
}