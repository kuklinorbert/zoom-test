package com.example.zoomtest.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.zoomtest.R
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var loginButton: Button = findViewById(R.id.navigateToLoginButton)
        var registerButton: Button = findViewById(R.id.navigateToRegisterButton)

        checkAuthentication()

        loginButton.setOnClickListener( View.OnClickListener {
            navigateToLogin()
        })

        registerButton.setOnClickListener(View.OnClickListener { navigateToRegister() })
    }

    private fun navigateToLogin(){
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun navigateToRegister(){
        startActivity(Intent(this, RegistrationActivity::class.java))
    }

    private fun checkAuthentication(){
        val user = Firebase.auth.currentUser
        if (user != null){
            startActivity(Intent(applicationContext,AuthenticatedActivity::class.java))
            finish()
        }
    }

}