package com.example.zoomtest.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.zoomtest.Contract.LoginContract
import com.example.zoomtest.Presenter.LoginPresenter
import com.example.zoomtest.R
import com.google.firebase.auth.FirebaseUser

class LoginActivity : AppCompatActivity(), LoginContract.View {
    private lateinit var loginPresenter: LoginPresenter
    private lateinit var inputEmail: EditText
    private lateinit var inputPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginPresenter = LoginPresenter(this)
        var loginButton: Button = findViewById(R.id.loginButton)
        var forgotPasswordButton : Button = findViewById(R.id.navigateToForgotPasswordButton)

        inputEmail = findViewById(R.id.inputEmail)
        inputPassword = findViewById(R.id.inputPassword)

        loginButton.setOnClickListener {
            login()
        }

        forgotPasswordButton.setOnClickListener {
            navigateToForgotPassword()
        }
    }
    private fun login(){
        loginPresenter.login(inputEmail.text.toString(),inputPassword.text.toString())
    }

    private fun navigateToForgotPassword(){
        startActivity(Intent(applicationContext, ForgotPasswordActivity::class.java))
    }

    override fun loginSuccess(firebaseUser: FirebaseUser) {
        var intent: Intent = Intent(this, AuthenticatedActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun loginFail(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
    }
}