package com.example.zoomtest.View

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.zoomtest.Contract.UserContract
import com.example.zoomtest.Model.User
import com.example.zoomtest.Presenter.UserPresenter
import com.example.zoomtest.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat

class AuthenticatedActivity : AppCompatActivity(), UserContract.View {
    private lateinit var userPresenter: UserPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authenticated)

        userPresenter = UserPresenter(this)
        userPresenter.getData(FirebaseAuth.getInstance().uid.toString())
        var signOutButton: Button = findViewById(R.id.signOutButton)


        signOutButton.setOnClickListener(View.OnClickListener {
            signOut()
        })
    }

    private fun signOut(){
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }

    @SuppressLint("SetTextI18n")
    override fun getDataSuccess(user: User) {
        val dateFormat = SimpleDateFormat("dd.MM.yyyy")
        var date = dateFormat.format(user.birthDate.time).toString()
        user.birthDate.time
        var userDataTextView: TextView = findViewById(R.id.textViewSuccess)
        userDataTextView.text = "Felhasználó adatai: \n ${user.name} \n ${user.email} \n ${user.phoneNumber} \n ${date}"
         "/n"
    }

    override fun getDataFail(message: String) {
        Toast.makeText(applicationContext,message,Toast.LENGTH_LONG).show()
    }
}