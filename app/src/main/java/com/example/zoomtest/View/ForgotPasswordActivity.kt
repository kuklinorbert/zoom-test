package com.example.zoomtest.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.zoomtest.Contract.ForgotPasswordContract
import com.example.zoomtest.Presenter.ForgotPasswordPresenter
import com.example.zoomtest.R

class ForgotPasswordActivity : AppCompatActivity(), ForgotPasswordContract.View {
    private lateinit var forgotPasswordPresenter: ForgotPasswordPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        forgotPasswordPresenter = ForgotPasswordPresenter(this)

        var forgotPasswordButton: Button = findViewById(R.id.forgotPasswordButton)

        forgotPasswordButton.setOnClickListener {
            sendPasswordReset()
        }
    }

    private fun sendPasswordReset(){
        var forgotPasswordEmail: EditText = findViewById(R.id.editTextForgot)
        forgotPasswordPresenter.resetPassword(forgotPasswordEmail.text.toString())
    }

    override fun sendSuccess() {
        Toast.makeText(applicationContext,"Email elküldve!",Toast.LENGTH_LONG).show()
    }

    override fun sendFail(message: String) {
        Toast.makeText(applicationContext,message,Toast.LENGTH_LONG).show()
    }
}