package com.example.zoomtest.View

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zoomtest.Contract.RegisterContract
import com.example.zoomtest.Presenter.RegisterPresenter
import com.example.zoomtest.R
import com.google.firebase.auth.FirebaseUser
import java.text.SimpleDateFormat
import java.util.*


class RegistrationActivity : AppCompatActivity(), RegisterContract.View {
    private lateinit var registerPresenter: RegisterPresenter
    private lateinit var inputEmail: EditText
    private lateinit var inputPassword: EditText
    private lateinit var inputPasswordAgain: EditText
    private lateinit var inputName: EditText
    private lateinit var inputPhoneNumber: EditText
    private lateinit var inputBirthDate: EditText
    private val calendar = Calendar.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        var registrationButton : Button = findViewById(R.id.registerButton)
        inputEmail = findViewById(R.id.formInputEmail)
        inputPassword = findViewById(R.id.formInputPassword)
        inputPasswordAgain = findViewById(R.id.formInputPasswordAgain)
        inputName = findViewById(R.id.formInputName)
        inputPhoneNumber = findViewById(R.id.formInputPhone)
        inputBirthDate = findViewById(R.id.formInputDate)
        registerPresenter = RegisterPresenter(this)


        var date =
            OnDateSetListener { view, year, month, day ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, month)
                calendar.set(Calendar.DAY_OF_MONTH, day)
                val dateFormat = SimpleDateFormat("dd.MM.yyyy")
                inputBirthDate.setText(dateFormat.format(calendar.time).toString())
            }

        inputBirthDate.setOnClickListener{
            var datePickerDialog = DatePickerDialog(this,date,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH))
            datePickerDialog.datePicker.maxDate = Date().time
            datePickerDialog.show()
        }
        registrationButton.setOnClickListener { register() }
    }

    private fun register(){
        var checkBox : CheckBox = findViewById(R.id.checkBox)
        if(inputName.text.isEmpty() || inputEmail.text.isEmpty() || inputPhoneNumber.text.isEmpty() || inputBirthDate.text.isEmpty() || inputPassword.text.isEmpty()){
            Toast.makeText(applicationContext,"Üres mező észlelve",Toast.LENGTH_LONG).show()
        } else if (inputName.text.contains(Regex("[^A-Za-zÁÉÍÓÖŐÚÜŰáéíóöőúüű ]"))) {
            Toast.makeText(applicationContext,"A név nem tartalmazhat nagybetűt vagy speciális karaktert",Toast.LENGTH_LONG).show()
        }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(inputEmail.text).matches()){
            Toast.makeText(applicationContext,"Rossz email formátum",Toast.LENGTH_LONG).show()
        }else if(!(inputPassword.text.length >= 8)){
            Toast.makeText(applicationContext,"A jelszónak legalább 8 karakter hosszúnak kell lennie",Toast.LENGTH_LONG).show()
        } else if(!(inputPassword.text.contains(Regex("[A-ZÁÉÍÓÖŐÚÜŰ]")) && inputPassword.text.contains(Regex("[0-9]")))){
            Toast.makeText(applicationContext,"A jelszónak tartalmaznia kell nagybetűt és számot",Toast.LENGTH_LONG).show()
        } else if(inputPassword.text.toString() != inputPasswordAgain.text.toString()){
            Toast.makeText(applicationContext,"A két jelszó nem egyezik",Toast.LENGTH_LONG).show()
        }else if(!checkBox.isChecked) {
            Toast.makeText(applicationContext,"Felhasználási feltételek nem lettek elfogadva",Toast.LENGTH_LONG).show()
        } else{
            registerPresenter.register(inputEmail.text.toString(),inputPassword.text.toString(),inputName.text.toString(),inputPhoneNumber.text.toString(),calendar.time)
        }

        }



    override fun registerSuccess(firebaseUser: FirebaseUser) {
        var intent: Intent = Intent(this, AuthenticatedActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun registerFail(message: String) {
        Toast.makeText(applicationContext, message,Toast.LENGTH_LONG).show()
    }


}