package com.example.zoomtest.Contract

import com.example.zoomtest.Model.User

interface UserContract {
    interface Model{
        interface UserDataListener{
            fun onSuccess(user: User)
            fun onFailure(message :String)
        }
        fun getUserData(listener: UserDataListener, uid: String)
    }
    interface View {
        fun getDataSuccess(user: User)
        fun getDataFail(message: String)
    }
    interface Presenter {
        fun getData(uid: String)
    }
}