package com.example.zoomtest.Contract

import com.google.firebase.auth.FirebaseUser


interface LoginContract {
    interface Model {
        interface LoginListener {
            fun onSuccess(firebaseUser: FirebaseUser?)
            fun onFailure(message: String)
        }

        fun loginUser(listener: LoginListener, email: String, password: String)
    }

    interface View {
        fun loginSuccess(firebaseUser: FirebaseUser)
        fun loginFail(message: String)
    }

    interface Presenter {
        fun login(email: String, password: String)
    }
}
