package com.example.zoomtest.Contract

import com.google.firebase.auth.FirebaseUser

interface ForgotPasswordContract {
    interface Model {
        interface LoginListener {
            fun onSuccess()
            fun onFailure(message: String)
        }

        fun sendPasswordReset(listener: LoginListener, email: String)
    }

    interface View {
        fun sendSuccess()
        fun sendFail(message: String)
    }

    interface Presenter {
        fun resetPassword(email: String)
    }
}