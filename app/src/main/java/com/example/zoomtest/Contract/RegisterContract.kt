package com.example.zoomtest.Contract

import com.google.firebase.auth.FirebaseUser
import java.util.*

interface RegisterContract {
    interface Model{
        interface RegisterListener{
            fun onSuccess(firebaseUser: FirebaseUser?)
            fun onFailure(message :String)
        }
        fun registerUser(listener: RegisterListener, email:String, password: String, name: String, phoneNumber: String, birthDate: Date)
    }
    interface View {
        fun registerSuccess(firebaseUser: FirebaseUser)
        fun registerFail(message: String)
    }
    interface Presenter {
        fun register(email: String, password: String, name: String, phoneNumber: String, birthDate: Date)
    }
}